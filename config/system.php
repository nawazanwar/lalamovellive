<?php
return [
    'order_hook_callback_url' => 'https://logiceverest.com/lalamovelive/public/order/hook/callback',
    'absolute_url' => 'https://logiceverest.com/lalamovelive/public/',
    'site_url' => 'https://logiceverest.com/lalamovelive/',
    'app_name' => 'lalamove-shipment',
    'api_version' => '2020-04',
    'redirect_uri' => 'https://logiceverest.com/lalamovelive/public/generate_token',
    'api_key' => '93c906fbd9dc96e6508b24ea645888b7',
    'secret_key' => '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',
    'scopes' => 'read_content,write_content,read_themes,write_themes,read_script_tags,write_script_tags,read_products,write_products,read_product_listings,read_inventory,write_inventory,write_checkouts,read_reports,write_reports,read_price_rules,write_price_rules,read_discounts,write_discounts,read_shopify_payments_payouts,read_customers,read_checkouts,write_checkouts,write_customers,read_orders,write_orders,read_draft_orders,write_draft_orders,read_inventory,write_inventory,read_fulfillments,write_fulfillments,read_assigned_fulfillment_orders,write_assigned_fulfillment_orders,read_merchant_managed_fulfillment_orders,write_merchant_managed_fulfillment_orders,read_third_party_fulfillment_orders,write_third_party_fulfillment_orders,read_shipping,write_shipping,read_analytics'
];
