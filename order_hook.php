<?php
@include "custom/connection.php";
@include "lala/vendor/autoload.php";
date_default_timezone_set('Asia/Singapore');

$order = file_get_contents('php://input');
$order = json_decode($order, true);

$financial_status = $order['financial_status'];

$payment_method = null;
$shipping_lines = $order['shipping_lines'];
$total_fee = null;

foreach ($shipping_lines as $shipping) {

    if ($shipping['title'] == 'LalaMove Delivery') {
        $payment_method = "is_valid";
        $total_fee = $shipping['price'];
    }
}
file_put_contents('payment_method.txt', $payment_method);

if ($financial_status == 'paid' && $payment_method == 'is_valid') {


    $already_found_order_query = "SELECT * FROM `orders` WHERE `shopify_order_name`='{$order['name']}' AND `shop_id`='{$shop_id}' ";
    $already_found_order_result = $conn->query($already_found_order_query);
    if ($already_found_order_result->num_rows < 1) {
        $specialRequests = array();
        if (isset($order['gateway']) && $order['gateway'] == 'Cash on Delivery (COD)') {
            array_push($specialRequests, 'COD');
        }
        $remarks_array = array();
        if (isset($order['name']) && $order['name'] != '') {
            array_push($remarks_array, $order['name']);
            if (isset($order['line_items']) && count($order['line_items']) > 0) {
                foreach ($order['line_items'] as $line_item) {
                    array_push($remarks_array, $line_item['name'] . " x " . $line_item['quantity']);
                }
            }
        }
        $orderRemarks = implode(', ', $remarks_array);
        $shipping_address = $order['shipping_address'];
        $address = $shipping_address['address1'] . ' ' . $shipping_address['city'] . ' ' . $shipping_address['zip'];
        $lang_lat_array = get_lat_long($address);
        $totalFee = 0;
        $shipping_lines = $order['shipping_lines'];

        if ($shipping_lines[0]['title'] == 'LalaMove Delivery') {

            $totalFee = $shipping_lines[0]['price'];
            $body = array(
                "serviceType" => "MOTORCYCLE",
                "specialRequests" => $specialRequests,
                "requesterContact" => array(
                    "name" => isset($setting['person']) ? $setting['person'] : "",
                    "phone" => isset($setting['phone']) ? $setting['phone'] : ""
                ),
                "stops" => array(
                    array(
                        "location" =>
                            array(
                                "lat" => isset($setting['lat']) ? $setting['lat'] : "",
                                "lng" => isset($setting['lng']) ? $setting['lng'] : ""
                            ),
                        "addresses" => array(
                            "en_" . $country_code => array(
                                "displayString" => isset($setting['address']) ? $setting['address'] : "",
                                "country" => $country_code
                            )
                        )
                    ),
                    array(
                        "location" => array(
                            "lat" => trim(isset($lang_lat_array['lat']) ? $lang_lat_array['lat'] : ""),
                            "lng" => trim(isset($lang_lat_array['lng']) ? $lang_lat_array['lng'] : "")
                        ),
                        "addresses" => array(
                            "en_" . $country_code => array(
                                "displayString" => isset($shipping_address['address1']) ? $shipping_address['address1'] : "",
                                "country" => $country_code
                            )
                        )
                    )
                ),
                "deliveries" => array(
                    array(
                        "toStop" => 1,
                        "toContact" => array(
                            "name" => (isset($shipping_address['name']) && $shipping_address['name'] != '') ? $shipping_address['name'] : $setting['person'],
                            "phone" => (isset($shipping_address['phone']) && $shipping_address['phone'] != '') ? $shipping_address['phone'] : $setting['phone']
                        ),
                        "remarks" => $orderRemarks
                    )
                )
            );

            $note_attributes = $order['note_attributes'];
            $delivery_datetime = null;
            if (count($note_attributes) > 0) {
                foreach ($note_attributes as $note_attribute) {
                    if ($note_attribute['name'] == 'delivery_datetime' && $note_attribute['value'] != '') {
                        $delivery_datetime = $note_attribute['value'];
                        $body['scheduleAt'] = gmdate('Y-m-d\TH:i:s\Z', strtotime($delivery_datetime));
                        // $body['scheduleAt'] = date('c', strtotime($delivery_datetime));
                    }
                }
            }

            $request = new \Lalamove\Api\LalamoveApi($api_url, $api_key, $api_secret, $country_code);

            $quotation = $request->quotation($body)->getBody()->getContents();
            $quotation = json_decode($quotation, true);

            $body['quotedTotalFee'] = array(
                "amount" => $quotation['totalFee'],
                "currency" => $setting['currency']
            );


            /* Ready to post a Order*/
            $order_result = $request->postOrder($body);
            $order_result_status = $order_result->getStatusCode();
            $order_result_content = $order_result->getBody()->getContents();

            if ($order_result_status == '200') {

                $api_response = json_decode($order_result_content, true);
                /* Ready to Save the Order*/
                $order_query = "INSERT INTO
                            `orders`(
                                    `shop_id`,
                                    `shopify_order_id`,
                                    `shopify_order_name`,
                                    `order_detail`,
                                    `lala_order_id`,
                                    `lala_order_ref`
                                 ) VALUES (
                                    '" . $shop_id . "',
                                    '" . $order['id'] . "',
                                    '" . $order['name'] . "',
                                    '" . json_encode($order) . "',
                                    '" . $api_response['customerOrderId'] . "',
                                    '" . $api_response['orderRef'] . "'
                                 )";
                if ($conn->query($order_query) === TRUE) {

                    $last_id = $conn->insert_id;

                    $location_id = null;
                    $locations_array = call_shopify($shop_name, $shopify_token, "/admin/api/2020-04/locations.json", array(), 'GET')['response'];
                    $locations = json_decode($locations_array, true)['locations'];
                    foreach ($locations as $location) {
                        if ($location['active'] == 'true') {
                            $location_id = $location['id'];
                        }
                    }

                    /*Ready to create the Order Full Fillment*/

                    $fulfillment_query_params = [
                        "fulfillment" => [
                            'location_id' => $location_id,
                            "tracking_company" => 'Lalamove',
                            "tracking_number" => $api_response['orderRef'],
                            "notify_customer" => true
                        ]
                    ];

                    $fulfillment_url = "/admin/api/2020-04/orders/" . $order['id'] . "/fulfillments.json";
                    $fulfillment_encode = call_shopify($shop_name, $shopify_token, $fulfillment_url, $fulfillment_query_params, 'POST')['response'];
                    $fulfillment_decode = json_decode($fulfillment_encode, true);

                    if (isset($fulfillment_decode['fulfillment'])) {
                        $update_shopify_fullfillment_query = "UPDATE `orders` SET `shopify_fulfillment_id`='" . $fulfillment_decode['fulfillment']['id'] . "' WHERE `id`=" . $last_id;
                        $conn->query($update_shopify_fullfillment_query);
                    }

                }//end of the run order query
            }//end of order status

        }//end of the check (method is Lalamove Delivery)

    }//end of the condition
}
