<?php

@include "custom/connection.php";
@include "lala/vendor/autoload.php";

$input = file_get_contents('php://input');
// parse the request
$rates = json_decode($input, true);

$destination = $rates['rate']['destination'];
$lang_lat_array = get_lat_long($destination['address1'].' '.$destination['city'].' '.$destination['zip']);
$body = array(
    "serviceType" => "MOTORCYCLE",
    "specialRequests" => array(),
    "requesterContact" => array(
        "name" => isset($setting['person']) ? $setting['person'] : "",
        "phone" => isset($setting['phone']) ? $setting['phone'] : ""
    ),
    "stops" => array(
        array(
            "location" =>
                array(
                    "lat" => isset($setting['lat']) ? $setting['lat'] : "",
                    "lng" => isset($setting['lng']) ? $setting['lng'] : ""
                ),
            "addresses" => array(
                "en_" . $country_code => array(
                    "displayString" => isset($setting['address']) ? $setting['address'] : "",
                    "country" => $country_code
                )
            )
        ),
        array(
            "location" => array(
                "lat" => trim(isset($lang_lat_array['lat']) ? $lang_lat_array['lat'] : ""),
                "lng" => trim(isset($lang_lat_array['lng']) ? $lang_lat_array['lng'] : "")
            ),
            "addresses" => array(
                "en_" . $country_code => array(
                    "displayString" => isset($destination['address1']) ? $destination['address1'] : "",
                    "country" => $country_code
                )
            )
        )
    ),
    "deliveries" => array(
        array(
            "toStop" => 1,
            "toContact" => array(
                "name" => (isset($destination['name']) && $destination['name'] != '') ? $destination['name'] : $setting['person'],
                "phone" => (isset($destination['phone']) && $destination['phone'] != '') ? $destination['phone'] : $setting['phone']
            ),
            "remarks" => ""
        )
    )
);

$request = new \Lalamove\Api\LalamoveApi($api_url, $api_key, $api_secret, $country_code);
$result = $request->quotation($body);
if ($result->getStatusCode() == '200') {

    $api_response = json_decode($result->getBody()->getContents(), true);
    $totalFee = $api_response['totalFee'] * 100;
    $output = array('rates' => array(
        array(
            'service_name' => 'LalaMove Delivery',
            'service_code' => 'Lala',
            'total_price' => $totalFee,
            'currency' => $api_response['totalFeeCurrency'],
            'min_delivery_date' => date('Y-m-d H:i:s O'),
            'max_delivery_date' => date('Y-m-d H:i:s O')
        )
    ));
    $json_output = json_encode($output);
    print $json_output;

}

?>
