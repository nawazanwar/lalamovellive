<?php
$shop_name = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
$conn = new mysqli("localhost", "iamusama_lalamovelive_user", "EJ*23wIQW]fj", "iamusama_lalamovelive");

$shop_id = null;
$api_key = null;
$api_secret = null;
$api_url = null;
$country_code = null;
$setting = array();
$shopify_token = null;
/*get the shop id*/

$shop_query = "SELECT * FROM shops WHERE name='$shop_name'";
$shop_result = $conn->query($shop_query);
if ($shop_result->num_rows > 0) {
    $shop = $shop_result->fetch_assoc();
    $shop_id = $shop['id'];
    $shopify_token = $shop['access_token'];
}
/* Get the Settings*/
$setting_query = "SELECT * FROM `settings` LEFT JOIN `countries` on settings.country_id=countries.id WHERE `shop_id` = '$shop_id'";
$setting_result = $conn->query($setting_query);

if ($setting_result->num_rows > 0) {
    $setting = $setting_result->fetch_assoc();
    if ($setting['test_mode'] == '1') {

        $api_key = $setting['test_api_key'];
        $api_secret = $setting['test_api_secret'];
        $api_url = "https://sandbox-rest.lalamove.com";

    } else {

        $api_key = $setting['live_api_key'];
        $api_secret = $setting['live_api_secret'];
        $api_url = "https://rest.lalamove.com";

    }

    $country_code = isset($setting['code']) ? $setting['code'] : null;
}


function get_lat_long($address)
{
    $url = "https://maps.google.com/maps/api/geocode/json?address=" . urlencode($address) . "&key=AIzaSyC4KM8oF8-EwHa66m3N6h4dS-zb0mVtVx4";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $responseJson = curl_exec($ch);
    curl_close($ch);
    $response = json_decode($responseJson);

    if ($response->status == 'OK') {

        $latitude = $response->results[0]->geometry->location->lat;
        $longitude = $response->results[0]->geometry->location->lng;
        return array('lat' => $latitude, 'lng' => $longitude);

    } else {

        return array();

    }
}

// Call shopify Function
function call_shopify($shop_name, $access_token, $api_endpoint, $query = array(), $method = 'GET', $request_headers = array())
{
    // Build URL
    $url = "https://" . $shop_name . $api_endpoint;
    file_put_contents("fullfillment_final_url.txt",print_r($url,true));
    if (!is_null($query) && in_array($method, array('GET', 'DELETE'))) $url = $url . "?" . http_build_query($query);
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER, TRUE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_USERAGENT, 'My New Shopify App v.1');
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    $request_headers[] = "";
    if (!is_null($access_token)) $request_headers[] = "X-Shopify-Access-Token: " . $access_token;
    curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);
    if ($method != 'GET' && in_array($method, array('POST', 'PUT'))) {
        if (is_array($query)) $query = http_build_query($query);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $query);
    }
    $response = curl_exec($curl);
    $error_number = curl_errno($curl);
    $error_message = curl_error($curl);
    curl_close($curl);
    if ($error_number) {
        return $error_message;
    } else {
        $response = preg_split("/\r\n\r\n|\n\n|\r\r/", $response, 2);
        $headers = array();
        $header_data = explode("\n", $response[0]);
        $headers['status'] = $header_data[0];
        array_shift($header_data);
        foreach ($header_data as $part) {
            $h = explode(":", $part, 2);
            $headers[trim($h[0])] = trim($h[1]);
        }
        return array('headers' => $headers, 'response' => $response[1]);
    }
}
