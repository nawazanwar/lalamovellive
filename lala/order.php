<?php

include_once '../lala/vendor/autoload.php';


$body = array(
    "quotedTotalFee" => ["amount"=>"65", "currency"=>"THB"],
    "serviceType" => "MOTORCYCLE",
    "specialRequests" => array(),
    "requesterContact" => array(
        "name" => "Draco Yam",
        "phone" => "+6652344758"
    ),
    "stops" => array(
        array(
            "location" => array("lat" => "13.7413232", "lng" => "100.5506183"),
            "addresses" => array(
                "en_TH" => array(
                    "displayString" => "Hooters Nana, 4 Sukhumvit 4 Alley",
                    "country" => "TH"
                )
            )
        ),
        array(
            "location" => array("lat" => "13.7410314", "lng" => "100.55079"),
            "addresses" => array(
                "en_TH" => array(
                    "displayString" => "The Landmark Bangkok",
                    "country" => "TH"
                )
            )
        )
    ),
    "deliveries" => array(
        array(
            "toStop" => 1,
            "toContact" => array(
                "name" => "Brian Garcia",
                "phone" => "+6652344758"
            ),
            "remarks" => "Test ORDER #: 1234, ITEM 1 x 1, ITEM 2 x 2"
        )
    )
);

$key = 'ef0f2ab1813a4b13bf58c1d5026cc4c6';
$secret = 'MCwCAQACBQDQC31FAgMBAAECBCCoZEECAwD+8wIDANDnAgMA+U0CAl/DAgI1';
$path = 'https://sandbox-rest.lalamove.com';
$request = new \Lalamove\Api\LalamoveApi($path, $key, $secret, 'TH');
$result = $request->postOrder($body);
echo $result->getStatusCode();
echo '<pre>';
print_r(json_decode($result->getBody()->getContents()));
echo '</pre>';
