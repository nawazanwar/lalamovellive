<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Country;
use App\Models\Equili;
use App\Models\Image;
use App\Models\Product;
use App\Models\Shop;
use App\Models\Setting;
use App\Models\Shopify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $shop = $request->query('shop', null);
        $hmac = $request->query('hmac', null);
        $timestamp = $request->query('timestamp', null);
        if (Shop::where('name', $shop)->exists()) {
            //Check if App is already deleted by Hooks
            $token = Shop::where('name', $shop)->value('access_token');
            $hooks = json_decode(Shopify::call($token, $shop, '/admin/api/' . config('system.api_version') . '/webhooks.json', array("topic" => 'app/uninstalled'), 'GET')['response'], true);
            if (isset($hooks['errors']) && $hooks['errors'] == '[API] Invalid API key or access token (unrecognized login or wrong password)') {
                return redirect()->route('app.install', ['hmac' => $hmac, 'shop' => $shop, 'timestamp', $timestamp]);
            } else {
                //check request came from app or install url
                if ($request->has('session')) {
                    /*url come form app*/
                    $shopModel = Shop::where('name', $shop)->first();

                    return view('dashboard',
                        [
                            'shop' => $shopModel,
                            'setting' => Setting::where('shop_id', $shopModel->id)->first(),
                            'countries' => Country::all()
                        ]);
                } else {
                    /*url come form installation*/
                    $return_url = 'https://' . $shop . "/admin/apps/lalamove-shipment";
                    header("Location: " . $return_url);
                    die();
                }
            }
        } else {
            return redirect()->route('app.install', ['hmac' => $hmac, 'shop' => $shop, 'timestamp', $timestamp]);
        }
    }

    public function install(Request $request)
    {
        $shop = $request->query('shop', null);
        Shop::updateOrCreate(array('name' => $shop), array('name' => $shop, 'hmac' => $request->query('hmac')));
        $install_url = "https://" . $shop . "/admin/oauth/authorize?client_id=" . config('system.api_key') . "&scope=" . trim(config('system.scopes')) . "&redirect_uri=" . urlencode(config('system.redirect_uri'));
        header("Location: " . $install_url);
        die();
    }

    public function generate_token(Request $request)
    {
        $params = $_GET;
        $hmac = $_GET['hmac'];
        $params = array_diff_key($params, array('hmac' => ''));
        ksort($params);
        $computed_hmac = hash_hmac('sha256', http_build_query($params), config('system.secret_key'));
        if (hash_equals($hmac, $computed_hmac)) {
            $query = array(
                "client_id" => config('system.api_key'),
                "client_secret" => config('system.secret_key'),
                "code" => $params['code']
            );
            $access_token_url = "https://" . $params['shop'] . "/admin/oauth/access_token";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $access_token_url);
            curl_setopt($ch, CURLOPT_POST, count($query));
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
            $result = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($result, true);
            $access_token = $result['access_token'];
            // update the access token
            $shop = $request->query('shop', null);
            $config = Shop::where('name', $shop)->first();
            $config->access_token = $access_token;
            $config->save();

            /* Update the active theme ID*/
            $themes = Shopify::call($access_token, $params['shop'], "/admin/api/" . config('system.api_version') . "/themes.json", array(), 'GET')['response'];
            $themes = json_decode($themes, true)['themes'];
            $active_theme_id = '';
            foreach ($themes as $theme) {
                $active_theme_id = ($theme['role'] == 'main') ? $theme['id'] : '';
            }
            $config->active_theme_id = $active_theme_id;
            $config->save();

            /* Create the web hooks */

            $this->createOrderHook($config);
            $this->PaidOrderHook($config);
            $this->createCarrierService($config);
            $this->createScriptTag($config);
            $this->createJqueryTag($config);

            /*return back to the App*/
            $return_url = 'https://' . $config->name . "/admin/apps/lalamove-shipment";
            header("Location: " . $return_url);
            die();

        } else {
            die('This request is NOT from Shopify!');
        }
    }

    public function createCarrierService($shopModel)
    {
        $data = array(
            "carrier_service" => array(
                "name" => "Shipping Rate Provider",
                "callback_url" => "https://logiceverest.com/lalamovelive/carrier_service.php",
                "format" => "json",
                "service_discovery" => true
            )
        );
        $end_point = "/admin/api/" . config('system.api_version') . "/carrier_services.json";
        return Shopify::call($shopModel->access_token, $shopModel->name, $end_point, $data, 'POST')['response'];
    }

    public function createOrderHook($shopModel)
    {
        $data = array(
            'webhook' =>
                array(
                    'topic' => 'orders/create',
                    'address' => "https://logiceverest.com/lalamovelive/order_hook.php",
                    'format' => 'json'
                )
        );
        $end_point = "/admin/api/" . config('system.api_version') . "/webhooks.json";
        return Shopify::call($shopModel->access_token, $shopModel->name, $end_point, $data, 'POST')['response'];
    }

    public function createScriptTag($shopModel)
    {
        $data = array(
            'script_tag' =>
                array(
                    "event" => "onload",
                    'src' => config('system.absolute_url') . "js/lalamove_script_tag.js",
                    "display_scope" => "all"
                )
        );
        $end_point = "/admin/api/" . config('system.api_version') . "/script_tags.json";
        return Shopify::call($shopModel->access_token, $shopModel->name, $end_point, $data, 'POST')['response'];
    }

    public function createJqueryTag($shopModel)
    {
        $data = array(
            'script_tag' =>
                array(
                    "event" => "onload",
                    'src' => config('system.absolute_url') . "js/jquery.min.js",
                    "display_scope" => "all"
                )
        );
        $end_point = "/admin/api/" . config('system.api_version') . "/script_tags.json";
        return Shopify::call($shopModel->access_token, $shopModel->name, $end_point, $data, 'POST')['response'];
    }

    public function PaidOrderHook($shopModel)
    {
        $data = array(
            'webhook' =>
                array(
                    'topic' => 'orders/paid',
                    'address' => "https://logiceverest.com/lalamovelive/order_hook.php",
                    'format' => 'json'
                )
        );
        $end_point = "/admin/api/" . config('system.api_version') . "/webhooks.json";
        return Shopify::call($shopModel->access_token, $shopModel->name, $end_point, $data, 'POST')['response'];
    }
}

