<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Models\Setting;
use App\Models\Shop;
use App\Models\Shopify;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Lalamove\Api\LalamoveApi;

class CheckStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check_status:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Command is Related to the Check Status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        @include "lala/vendor/autoload.php";

        $not_don_orders = Order::where('is_don', false)->get();

        if (count($not_don_orders) > 0) {

            foreach ($not_don_orders as $order) {

                $shop_id = $order->shop_id;
                $setting = Setting::with('country')->where('shop_id', $shop_id)->get()[0];
                $api_key = null;
                $api_secret = null;
                $country_code = null;
                $api_url = null;

                if (isset($setting->test_mode) && $setting->test_mode == true) {

                    $api_key = $setting->test_api_key;
                    $api_secret = $setting->test_api_secret;
                    $api_url = "https://sandbox-rest.lalamove.com";

                } else {

                    $api_key = $setting->live_api_key;
                    $api_secret = $setting->live_api_secret;
                    $api_url = "https://rest.lalamove.com";
                }

                $country_code = isset($setting->country) ? $setting->country->code : '';

                $request = new \Lalamove\Api\LalamoveApi($api_url, $api_key, $api_secret, $country_code);
                $lala_order_info = $request->getOrderStatus($order->lala_order_id);
                $lala_order_code = $lala_order_info->getStatusCode();

                if ($lala_order_code == '200') {

                    $lala_order_status = json_decode($lala_order_info->getBody()->getContents(), true)['status'];

                    /*update order status*/
                    $orderModel = Order::find($order->id);
                    $orderModel->lala_order_status = $lala_order_status;
                    $orderModel->save();

                    /*
                     * LalaMove All Status
                     * Assigning Driver (ASSIGNING_DRIVER)
                     * On Going  (ON_GOING)
                     * Pick Up (PICKED_UP)
                     * Rejected (REJECTED)
                     * Cancelled (CANCELED)
                     * Expired  (EXPIRED)
                     * Completed (COMPLETED)
                    */

                    $pending_array = array("ASSIGNING_DRIVER", "ON_GOING", "PICKED_UP");
                    $cancelled_array = array("REJECTED", "CANCELED", "EXPIRED");
                    $completed_array = array("COMPLETED");

                    $shopify_fulfillment_status = $orderModel->shopify_fulfillment_status;
                    $is_don = $orderModel->is_don;


                    $shop = Shop::find($shop_id);

                    /*check if the status exists in pending array*/
                    if (in_array($lala_order_status, $pending_array)) {
                        $shopify_fulfillment_status = 'Pending';
                        $is_don = false;
                    }

                    /*check if the status exists in cancelled array*/
                    if (in_array($lala_order_status, $cancelled_array)) {
                        $shopify_fulfillment_status = 'Cancelled';
                        $is_don = true;
                        $this->updateFullFill($shop, $orderModel, 'cancel');
                    }

                    /*check if the status exists in complete array*/
                    if (in_array($lala_order_status, $completed_array)) {
                        $shopify_fulfillment_status = 'Completed';
                        $is_don = true;
                        $this->updateFullFill($shop, $orderModel, 'complete');
                    }

                    /* Update the local full_fillment status and locally set order has don*/
                    $orderModel->shopify_fulfillment_status = $shopify_fulfillment_status;
                    $orderModel->is_don = $is_don;
                    $orderModel->save();

                    Log::info(
                        "--api_key--" . $api_key .
                        "-- api_secret--" . $api_secret .
                        '-- Country Code--' . $country_code .
                        '-- Lala Order Code--' . $lala_order_code .
                        '-- Lala Order Status--' . $lala_order_status
                    );
                }


            }//End foreach orders array

        }//End of count(order)


    }

    public function updateFullFill($shop, $order, $type)
    {

        $query = "/admin/api/2020-04/orders/" . $order->shopify_order_id . "/fulfillments/" . $order->shopify_fulfillment_id . "/" . $type . ".json";

        Shopify::call($shop->access_token, $shop->name, $query, 'GET');

    }


}
