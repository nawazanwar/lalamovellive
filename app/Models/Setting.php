<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'person',
        'phone',
        'address',
        'lat',
        'lng',
        'test_api_key',
        'test_api_secret',
        'live_api_key',
        'live_api_secret',
        'test_mode',
        'shop_id',
        'country_id',
        'currency'
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }
}
